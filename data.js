const jsonFile = require("jsonfile-promised");
const fs = require("fs");

module.exports = {
  salvaDados(curso, tempoEstudado){
    let arquivoDoCurso = __dirname + '/data/' + curso + '.json';
    if(fs.existsSync(arquivoDoCurso)){
      this.adicionaTempoAoCurso(arquivoDoCurso,tempoEstudado);
    }else{
      this.criarArquivoDeCurso(arquivoDoCurso,{})
        .then(() =>{
          this.adicionaTempoAoCurso(arquivoDoCurso,tempoEstudado);
        });
    }
  },
  adicionaTempoAoCurso(arquivoDoCurso, tempoEstudado){
    let dados = {
      ultimoEstudo: new Date().toString(),
      tempo:tempoEstudado
    };
    jsonFile.writeFile(arquivoDoCurso, dados, {spaces: 2})
      .then(() =>{
        console.log("Dados salvos!");
      }).catch((err) => {
        console.log(err);
      });
  },
  criarArquivoDeCurso(nomeArquivo, conteudoArquivo){
    return jsonFile.writeFile(nomeArquivo, conteudoArquivo,{spaces: 2})
      .then(() =>{
        console.log("Arquivo Criado");
      }).catch((err) =>{
        console.log(err);
      });
  },
  pegaDados(curso){
    let arquivoDoCurso = __dirname + '/data/' + curso + '.json';

    return jsonFile.readFile(arquivoDoCurso);
  },
  pegaNomeDosCursos(){
    let arquivos = fs.readdirSync(__dirname+"/data");
    let cursos = arquivos.map( (arquivo) => {
      return arquivo.substr(0, arquivo.lastIndexOf("."));
    });

    return cursos;
  }
}
